# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Q-Sys Plugin for a password protection keypad

### How do I get set up? ###

Install: 
Just install the qplug file

Setup: 
The plugin can be set up offline or live.
In offline mode all settings (e.g. the passwords) must be made in the plugin properties.
In live mode all settings can be done on the running system, so the user can change the passwords during runtime if necessary.

### Who do I talk to? ###

m.hermann@bpsg.de.qscportaluser.org
